getData()

document.onclick = function(event) {
    let a = event.target.closest('a'); // (1)
    setPrev()
};

function setPrev() {
    fetch('http://localhost:3400/prev', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({"url": window.location.href}),
    })
    .catch((error) => {
        console.error('Error:', error);
    });
}

function getData() {
    fetch('http://localhost:3400/getPrev')
    .then(resp => resp.json())
    .then((prevData) => {
        sendData(prevData)
    })
    .catch((error) => {
        console.error('Error:', error);
    });
}

function sendData(prevData) {
    let prevDataUrl = prevData ? prevData.url : "";
    const data = {
        "currentUrl" : window.location.href,
        "referrer" : document.referrer,
        "prevUrl" : prevDataUrl,
    };

    fetch('http://localhost:3400/api', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    })
    .then((data) => {
        return data.json();
    })
    .then(data => {
        const value = data.category;

        chrome.runtime.sendMessage({
            action: 'updateIcon',
            value: value
        })
    })
    .catch((error) => {
        console.error('Error:', error);
    });
}
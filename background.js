chrome.runtime.onMessage.addListener(function (msg, sender, sendResponse) {
    if (msg.action === "updateIcon") {
        if (msg.value) {
            const path = `/assets/icons/${msg.value}.png`;
            chrome.browserAction.setIcon({path: path});
        }
    }
});
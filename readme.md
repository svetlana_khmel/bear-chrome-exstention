These repos are the same demo-projects:

* 1) Crome extention gets current url, and referer and sends them to the node server.
Then waits for the category name and shows the icon according to the category.
https://bitbucket.org/svetlana_khmel/bear-chrome-exstention/src/master/

* 2) Node server gets links from the chrome extention, sends link to the python server, 
wait from the python server a response with category and sends the category to the 
chrome extention.
https://bitbucket.org/svetlana_khmel/plain-server-get-json/src/master/

* 3) Python server gets link from node server and sends back to the node server.
https://bitbucket.org/svetlana_khmel/flask-server/src/master/



How to debug Google Crome Extention:
https://www.google.com/search?q=how+to+debug+google+chrome+extension&oq=how+to+debug+google+ex&aqs=chrome.1.69i57j0l6.13273j0j7&sourceid=chrome&ie=UTF-8#kpvalbx=_7rKEXuOfLrLJ1QGClan4Cw25
https://developer.chrome.com/extensions/tut_debugging

How To Make Chrome Extensions
https://www.youtube.com/watch?v=Ipa58NVGs_c
https://www.youtube.com/redirect?redir_token=cgIQ6v7lJbj3-o1SzZzP7N-jfCh8MTU4NTY2OTAyOUAxNTg1NTgyNjI5&q=https%3A%2F%2Fgithub.com%2Fshama%2Fletswritecode%2Ftree%2Fmaster%2Fhow-to-make-chrome-extensions&event=video_description&v=Ipa58NVGs_c
